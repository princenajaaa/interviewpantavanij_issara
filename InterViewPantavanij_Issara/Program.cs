﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterViewPantavanij_Issara
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                List<order> orders = LoadOrders(ConfigurationManager.AppSettings["ORDERS_PATH"]);

                VendingMachine vendingMachine = new VendingMachine();

                //vendingMachine.OrderItem("xx", 5.211);
                foreach(order order in orders)
                {
                    vendingMachine.OrderItem(order.ItemCode, order.Money);
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("Error : " + e.Message);
            }

            Console.ReadKey();
        }

        

        public static List<order> LoadOrders(string path)
        {
            List<order> OrderList = new List<order>();
            using (StreamReader reader = File.OpenText(path))
            {
                int lineCount = 1;
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string[] splitItems = line.Split(',');
                    double money;
                    if (splitItems.Count() != 2) throw new Exception("invalid order at line " + lineCount);
                    if (!double.TryParse(splitItems[1], out money)) throw new Exception("invalid order at line " + lineCount);

                    OrderList.Add(new order(splitItems[0], Convert.ToDouble(splitItems[1])));
                    lineCount++;
                }
            }
            return OrderList;
        }

    }

    class VendingMachine
    {
        private double money = 0.0;
        private List<item> items = LoadItems(ConfigurationManager.AppSettings["ITEMS_PATH"]);

        public void OrderItem(string ItemCode, double money)
        {
            try
            {
                item getItem = items.Where(t => t.Code == ItemCode).FirstOrDefault();
                if (getItem == null) throw new Exception(string.Format(@"Invalid selection! : Money in vending machine = {0}", this.money.ToString("0.00")));
                if (getItem.Qty == 0) throw new Exception(string.Format(@"{0}: Out of stock!", getItem.Name));
                if (getItem.price > money) throw new Exception("Not enough money!");

                if (getItem.price == money)
                {
                    Console.WriteLine(string.Format(@"Vending {0}", getItem.Name));
                }
                else
                {
                    Console.WriteLine(string.Format(@"Vending {0} with {1} change", getItem.Name, (money - getItem.price).ToString("#.00")));
                }
                getItem.Qty--;
                this.money = this.money + getItem.price;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static List<item> LoadItems(string path)
        {
            List<item> ItemList = new List<item>();
            using (StreamReader reader = File.OpenText(path))
            {
                int lineCount = 1;
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string[] splitItems = line.Split('|');
                    int qty;
                    double price;

                    if (splitItems.Count() != 4) throw new Exception("invalid item at line " + lineCount);
                    if (!Int32.TryParse(splitItems[2], out qty)) throw new Exception("cannot parse item QTY at line " + lineCount);
                    if (!double.TryParse(splitItems[3], out price)) throw new Exception("cannot parse item price at line " + lineCount);

                    ItemList.Add(new item(splitItems[0], splitItems[1], Convert.ToInt32(splitItems[2]), Convert.ToDouble(splitItems[3])));
                    lineCount++;
                }
            }
            return ItemList;
        }
    }

    class item
    {
        public item(string Name, string Code, int Qty, double price)
        {
            this.Name = Name;
            this.Code = Code;
            this.Qty = Qty;
            this.price = price;
        }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Qty { get; set; }
        public double price{ get; set; }
    }

    class order
    {
        public order(string ItemCode, double Money)
        {
            this.ItemCode = ItemCode;
            this.Money = Money;
        }
        public string ItemCode { get; set; }
        public double Money { get; set; }
    }
}
